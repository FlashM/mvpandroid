package com.example.antonmaksimov.mvpwithoutdagger.utils;

public class URL {
    public final static String BASE = "https://api.icndb.com";
    public final static String RANDOM_JOKE = "/jokes/random/";
    public final static String MULTIPLE_RANDOM_JOKES = RANDOM_JOKE + "{number}";
}
