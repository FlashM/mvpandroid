package com.example.antonmaksimov.mvpwithoutdagger.view;

public interface RandomJokeView extends JokeView {
    void showJoke(String jokeText);
}
