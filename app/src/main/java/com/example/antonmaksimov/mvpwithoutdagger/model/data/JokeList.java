package com.example.antonmaksimov.mvpwithoutdagger.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class JokeList extends Joke {

    @SerializedName("value")
    private ArrayList<Value> list = null;

    public ArrayList<Value> get() {
        return list;
    }
}
