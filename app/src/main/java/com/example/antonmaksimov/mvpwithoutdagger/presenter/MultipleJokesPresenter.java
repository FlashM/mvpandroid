package com.example.antonmaksimov.mvpwithoutdagger.presenter;

import com.example.antonmaksimov.mvpwithoutdagger.model.JokeService;
import com.example.antonmaksimov.mvpwithoutdagger.model.data.JokeList;
import com.example.antonmaksimov.mvpwithoutdagger.view.MultipleJokesView;

import org.jdeferred.AlwaysCallback;
import org.jdeferred.Deferred;
import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

public class MultipleJokesPresenter {

    private MultipleJokesView jokeView;

    public MultipleJokesPresenter(MultipleJokesView jokeView) {
        this.jokeView = jokeView;
    }

    private Deferred<JokeList, Void, ?> callback() {
        Deferred<JokeList, Void, ?> deferred = new DeferredObject<>();
        Promise<JokeList, Void, ?> promise = deferred.promise();
        promise.done(new DoneCallback<JokeList>() {
            @Override
            public void onDone(JokeList list) {
                jokeView.showJokeList(list.get());
            }
        }).fail(new FailCallback<Void>() {
            @Override
            public void onFail(Void v) {
                jokeView.showError();
            }
        }).always(new AlwaysCallback<JokeList, Void>() {
            @Override
            public void onAlways(Promise.State state, JokeList resolved, Void rejected) {
                jokeView.hideProgress();
            }
        });
        return deferred;

    }

    public void getJokeList() {
        jokeView.showProgress();
        JokeService.getMultipleJokes(callback());
    }

}
