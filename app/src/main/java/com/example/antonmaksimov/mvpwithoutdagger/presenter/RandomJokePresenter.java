package com.example.antonmaksimov.mvpwithoutdagger.presenter;

import com.example.antonmaksimov.mvpwithoutdagger.model.JokeService;
import com.example.antonmaksimov.mvpwithoutdagger.model.data.OneJoke;
import com.example.antonmaksimov.mvpwithoutdagger.view.RandomJokeView;

import org.jdeferred.AlwaysCallback;
import org.jdeferred.Deferred;
import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

public class RandomJokePresenter {

    private RandomJokeView jokeView;

    public RandomJokePresenter(RandomJokeView jokeView) {
        this.jokeView = jokeView;
    }

    private Deferred<OneJoke, Void, ?> callback() {
        Deferred<OneJoke, Void, ?> deferred = new DeferredObject<>();
        Promise<OneJoke, Void, ?> promise = deferred.promise();
        promise.done(new DoneCallback<OneJoke>() {
            @Override
            public void onDone(OneJoke joke) {
                jokeView.showJoke(joke.getValue().getText());
            }
        }).fail(new FailCallback<Void>() {
            @Override
            public void onFail(Void v) {
                jokeView.showError();
            }
        }).always(new AlwaysCallback<OneJoke, Void>() {
            @Override
            public void onAlways(Promise.State state, OneJoke resolved, Void rejected) {
                jokeView.hideProgress();
            }
        });
        return deferred;

    }

    public void getJoke() {
        jokeView.showProgress();
        JokeService.getRandomJoke(callback());
    }

}
