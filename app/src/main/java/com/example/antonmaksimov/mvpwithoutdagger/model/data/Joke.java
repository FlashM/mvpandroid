package com.example.antonmaksimov.mvpwithoutdagger.model.data;

import com.google.gson.annotations.SerializedName;

public class Joke {

    private String type;

    public String getType() {
        return type;
    }

    public class Value {
        @SerializedName("joke")
        private String text;

        public String getText() {
            return text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

}
