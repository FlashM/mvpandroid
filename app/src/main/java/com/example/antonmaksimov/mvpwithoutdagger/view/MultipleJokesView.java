package com.example.antonmaksimov.mvpwithoutdagger.view;

import com.example.antonmaksimov.mvpwithoutdagger.model.data.Joke;

import java.util.ArrayList;

public interface MultipleJokesView extends JokeView {
    void showJokeList(ArrayList<Joke.Value> list);
}
