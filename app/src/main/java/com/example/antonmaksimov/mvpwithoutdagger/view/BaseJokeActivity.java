package com.example.antonmaksimov.mvpwithoutdagger.view;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.antonmaksimov.mvpwithoutdagger.R;

import butterknife.BindView;

public abstract class BaseJokeActivity extends AppCompatActivity implements JokeView {

    @BindView(R.id.button_joke) Button buttonJoke;
    @BindView(R.id.button_switch_mode) Button buttonSwitchMode;
    @BindView(R.id.progress_bar) ProgressBar progressBar;

    public abstract void onButtonJokeClick();

    public abstract void onButtonSwitchModeClick();

    @Override
    public void showError() {
        Toast.makeText(this, "Oops, something went wrong", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        buttonJoke.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        buttonJoke.setEnabled(true);
    }
}
