package com.example.antonmaksimov.mvpwithoutdagger.model;

import com.example.antonmaksimov.mvpwithoutdagger.model.data.JokeList;
import com.example.antonmaksimov.mvpwithoutdagger.model.data.OneJoke;
import com.example.antonmaksimov.mvpwithoutdagger.utils.URL;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RestService {

    @GET(URL.RANDOM_JOKE)
    Call<OneJoke> getRandomJoke();

    @GET(URL.MULTIPLE_RANDOM_JOKES)
    Call<JokeList> getMultipleRandomJokes(@Path("number") int numberOfJokes);
}
