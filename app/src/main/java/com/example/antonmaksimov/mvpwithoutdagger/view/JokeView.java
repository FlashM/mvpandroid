package com.example.antonmaksimov.mvpwithoutdagger.view;

public interface JokeView {
    void showProgress();

    void hideProgress();

    void showError();
}
