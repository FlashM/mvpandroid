package com.example.antonmaksimov.mvpwithoutdagger.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.antonmaksimov.mvpwithoutdagger.R;
import com.example.antonmaksimov.mvpwithoutdagger.model.data.Joke;
import com.example.antonmaksimov.mvpwithoutdagger.model.data.JokeList;
import com.example.antonmaksimov.mvpwithoutdagger.presenter.MultipleJokesPresenter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MultipleJokesActivity extends BaseJokeActivity implements MultipleJokesView {

    @BindView(R.id.list) ListView listView;

    MultipleJokesPresenter presenter;

    ArrayAdapter<Joke.Value> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joke_list);
        initViews();
        presenter = new MultipleJokesPresenter(this);
    }

    protected void initViews() {
        ButterKnife.bind(this);
        buttonJoke.setText(R.string.button_get_random_jokes);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        listView.setAdapter(adapter);
    }

    @OnClick(R.id.button_switch_mode)
    public void onButtonSwitchModeClick() {
        startActivity(new Intent(MultipleJokesActivity.this, RandomJokeActivity.class));
        finish();
    }

    @OnClick(R.id.button_joke)
    public void onButtonJokeClick() {
        presenter.getJokeList();
    }

    @Override
    public void showProgress() {
        super.showProgress();
        listView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
        listView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showJokeList(ArrayList<Joke.Value> list) {
        adapter.clear();
        adapter.addAll(list);
    }
}
