package com.example.antonmaksimov.mvpwithoutdagger.model;

import com.example.antonmaksimov.mvpwithoutdagger.utils.URL;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static RestService service;

    public static synchronized RestService getApi() {
        if (service == null) {

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            OkHttpClient okClient = builder.build();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(URL.BASE)
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            service = client.create(RestService.class);
        }
        return service;
    }

}
