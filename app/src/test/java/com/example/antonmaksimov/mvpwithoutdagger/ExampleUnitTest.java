package com.example.antonmaksimov.mvpwithoutdagger;

import com.example.antonmaksimov.mvpwithoutdagger.view.RandomJokeView;
import com.example.antonmaksimov.mvpwithoutdagger.presenter.RandomJokePresenter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    private RandomJokeView listener;

    private RandomJokePresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new RandomJokePresenter(listener);
    }

    @Test
    public void jokeLoadIsSuccessful() {
//        presenter.getJoke();
        assertNull(listener);
    }


}