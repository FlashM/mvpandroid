package com.example.antonmaksimov.mvpwithoutdagger.model;

import com.example.antonmaksimov.mvpwithoutdagger.model.data.JokeList;
import com.example.antonmaksimov.mvpwithoutdagger.model.data.OneJoke;

import org.jdeferred.Deferred;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JokeService {

    private final static int NUMBER_OF_JOKES = 10;

    public static void getRandomJoke(final Deferred<OneJoke, Void, ?> deferred) {
        RestClient.getApi().getRandomJoke().enqueue(new Callback<OneJoke>() {
            @Override
            public void onResponse(Call<OneJoke> call, Response<OneJoke> response) {
                if (response.isSuccessful()) {
                    deferred.resolve(response.body());
                } else {
                    deferred.reject(null);
                }
            }

            @Override
            public void onFailure(Call<OneJoke> call, Throwable t) {
                deferred.reject(null);
            }
        });
    }

    public static void getMultipleJokes(final Deferred<JokeList, Void, ?> deferred) {
        RestClient.getApi().getMultipleRandomJokes(NUMBER_OF_JOKES).enqueue(new Callback<JokeList>() {
            @Override
            public void onResponse(Call<JokeList> call, Response<JokeList> response) {
                if (response.isSuccessful()) {
                    deferred.resolve(response.body());
                } else {
                    deferred.reject(null);
                }
            }

            @Override
            public void onFailure(Call<JokeList> call, Throwable t) {
                deferred.reject(null);
            }
        });
    }

}
