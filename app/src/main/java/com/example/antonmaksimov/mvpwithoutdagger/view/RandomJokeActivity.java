package com.example.antonmaksimov.mvpwithoutdagger.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.antonmaksimov.mvpwithoutdagger.R;
import com.example.antonmaksimov.mvpwithoutdagger.model.data.OneJoke;
import com.example.antonmaksimov.mvpwithoutdagger.presenter.RandomJokePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RandomJokeActivity extends BaseJokeActivity implements RandomJokeView {

    @BindView(R.id.text_joke) TextView textViewJoke;

    RandomJokePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_joke);
        ButterKnife.bind(this);
        presenter = new RandomJokePresenter(this);
    }

    @OnClick(R.id.button_joke)
    public void onButtonJokeClick() {
        presenter.getJoke();
    }

    @OnClick(R.id.button_switch_mode)
    public void onButtonSwitchModeClick() {
        startActivity(new Intent(RandomJokeActivity.this, MultipleJokesActivity.class));
        finish();
    }

    @Override
    public void showJoke(String joke) {
        textViewJoke.setText(joke);
    }

    @Override
    public void showProgress() {
        super.showProgress();
        textViewJoke.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
        textViewJoke.setVisibility(View.VISIBLE);
    }
}
